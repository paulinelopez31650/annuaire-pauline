<?php

/**
 * @package Ajout entreprise
 * @version 1.7.2
 */
/*
Plugin Name: Ajout entreprise
Description: interface graphique dans le backoffice Wordpress,pour éditer les entreprises.
*/

// doc: https://www.undefined.fr/comment-creer-un-module-wordpress

class AjoutEntrepriseClass 
{
    public function __construct()
    {   
        add_action( 'admin_menu', [ $this, 'admin_ajout_plugin_menu'] );
    }

public function admin_ajout_plugin_menu()
{
        add_menu_page(
	    __('Ajout entreprise', 'mon_ajout-entreprise'), // Page titre
	    __('Ajout entreprise', 'mon_ajout-entreprise'), // Menu titre
	    'manage_options',  // Capability
	    'mon_ajout-entreprise', // Slug
	    [ &$this, 'load_ajout_plugin_page'], // Callback page function
	);
}

        public function load_ajout_plugin_page() 
        { 
                echo '<h1>' . __( 'Ajout entreprise', 'ajout-entreprise' ) . '</h1>'; 
                echo '<p>' . __( "Bienvenue dans l'interface d'ajout des entreprises.", 'ajout-entreprise' ) . '</p>'; 
        }
}

new AjoutEntrepriseClass();


