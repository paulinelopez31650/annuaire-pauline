<?php

/**
 * @package Annuaire
 * @version 1.7.2
 */
/*
Plugin Name: Annuaire
Plugin URI: http://wordpress.org/plugins/
Description: Cette extention permet d'afficher des données se trouvant dans la base de données.
*/

// This just echoes the chosen line, we'll position it later.

// 1st Method - Declaring $wpdb as global and using it to execute an SQL query statement that returns a PHP object

function shortcode_annuaire($atts)
{
    global $wpdb;
    $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}annuaire", OBJECT);
    ob_start();
    foreach ($results as $entreprise) {
?>
<li><?=$entreprise->nom_entreprise ?></li><p><?=$entreprise->localisation_entreprise ?></p>
<?php
    }
}
add_shortcode('annuaire', 'shortcode_annuaire');
